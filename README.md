## Hexagon Dodge

![Hexagon Dodge](screenshots/screenshot000.png "Hexagon Dodge")

### Description

Rotate the hexagons to dodge them

### Links

 - https://hexadeque.itch.io/hexagon-dodge

### License

This game sources are licensed under an unmodified zlib/libpng license, which is an OSI-certified, BSD-like license that allows static linking with closed source software. Check [LICENSE](LICENSE) for further details.

*Copyright (c) 2022 hexadeque*
