#include "raylib.h"

const int DEFAULT_SCREEN_WIDTH = 1280;
const int DEFAULT_SCREEN_HEIGHT = 720;

const Color BACKGROUND_COLOR = LIGHTGRAY;
const Color FOREGROUND_COLOR = WHITE;

const int CAMERA_SCALE = 20;
const Camera CAMERA = { { 0, 10, 0 }, { 0, 0, 0 }, { 0, 0, -1 }, CAMERA_SCALE, CAMERA_ORTHOGRAPHIC };

const float PLAYER_SIZE = 0.5;

const float HEXAGON_APOTHEM = 4;
const float HEXAGON_THICKNESS = 0.6;
const float HEXAGON_SPEED = 0.75;
const float HEXAGON_SPACING = 1;
#define HEXAGON_AMOUNT 6

const int ROTATION_SPEED = 120;

const int HUE_CHANGE_SPEED = 5;

const float DEATH_LENGTH = 0.5;
const float DEAD_PARTICLE_SIZE = 0.25;
const float DEAD_PARTICLE_SPREAD = 2;

const int SCORE_SIZE = 1;
const int SCORE_SPACE = 1;
