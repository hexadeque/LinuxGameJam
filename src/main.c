#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include "constants.h"

#if defined(PLATFORM_WEB)
    #include <emscripten/emscripten.h>
#endif

static Mesh GenSideMesh(void);
static void ResetHexagon(int);
static void StartGame(void);
static void EndGame(void);
static Vector2 PointFromRotation(int, float, float);

Model sideModel;
Music music;

struct
{
    float life[HEXAGON_AMOUNT];
    int start[HEXAGON_AMOUNT];
    int end[HEXAGON_AMOUNT];
} hexagons;

float rotation;
int score;
int highScore = 0;

enum
{
    StartScreen,
    Playing,
    Dead
} state;

float deadTime;

static void Draw(float deltaTime)
{
    static float hue;

    Color backgroundColor = ColorFromHSV(hue, 0.7, 0.7);
    Color foregroundColor = ColorFromHSV(hue, 0.5, 1);

    BeginDrawing();
        
        ClearBackground(backgroundColor);

        int width = GetScreenWidth();
        int height = GetScreenHeight();
        int scale = height / 20;

        if (state == Playing || state == StartScreen)
            DrawCircle(width / 2, height / 2, PLAYER_SIZE * scale, foregroundColor);

        switch (state)
        {
            case Dead:
                deadTime += deltaTime;

                for (int i = 0; i < 6; i++)
                {
                    float radius = sin(deadTime / DEATH_LENGTH * PI / 2) * DEAD_PARTICLE_SPREAD;

                    float x = sin(i * PI / 3 + 0.01) * radius;
                    float y = cos(i * PI / 3 + 0.01) * radius;

                    DrawCircle(x * scale + width / 2, y * scale + height / 2, DEAD_PARTICLE_SIZE * scale, foregroundColor);
                }

                if (deadTime >= DEATH_LENGTH)
                {
                    state = StartScreen;
                    
                    hue = rand() % 360;
                }
                
            case Playing:
                hue += HUE_CHANGE_SPEED * deltaTime;
                hue = fmod(hue, 360);

                BeginMode3D(CAMERA);
                    for (int i = 0; i < HEXAGON_AMOUNT; i++)
                    {
                        for (int j = 0; j < 6; j++)
                        {
                            if (j != hexagons.start[i] && j != hexagons.end[i])
                            {
                                    DrawModelEx(sideModel, (Vector3) { hexagons.life[i] * HEXAGON_SPACING * -CAMERA_SCALE / 2, 0, 0 }, (Vector3) { 0, 1, 0 }, j * 60 + rotation, (Vector3) { 1, 1, 1 }, foregroundColor);
                            }
                        }
                    }
                EndMode3D();

                char scoreText[5];
                sprintf(scoreText, "%d", score);

                int scoreWidth = MeasureText(scoreText, SCORE_SIZE * scale);

                DrawText(scoreText, (width - scoreWidth) / 2, SCORE_SPACE * scale, SCORE_SIZE * scale, foregroundColor);
                break;

            case StartScreen:
                DrawCircle(width / 2, height / 2, PLAYER_SIZE * height / 20, foregroundColor);

                int fontSize = height / 40;
                DrawText("A/D to rotate", height / 10, height / 2 - fontSize, fontSize, foregroundColor);
                DrawText("Space to start", height / 10, height / 2, fontSize, foregroundColor);

                char highScoreText[16];
                sprintf(highScoreText, "Highscore: %d", highScore);

                int highScoreWidth = MeasureText(highScoreText, fontSize);

                DrawText(highScoreText, width * 0.9 - highScoreWidth, (height - fontSize) / 2, fontSize, foregroundColor);
                break;
        }

    EndDrawing();
}

static void Logic(float deltaTime)
{
    if (state == StartScreen)
    {
        if (IsKeyPressed(KEY_SPACE))
        {
            StartGame();
        }

        return;
    }

    if (state == Dead)
        return;

    if (IsKeyDown(KEY_A))
    {
        rotation += ROTATION_SPEED * deltaTime;
    }
    if (IsKeyDown(KEY_D))
    {
        rotation -= ROTATION_SPEED * deltaTime;
    }

    if (rotation < 0)
    {
        rotation += 360;
    }

    for (int i = 0; i < HEXAGON_AMOUNT; i++)
    {
        float oldLife = hexagons.life[i];

        hexagons.life[i] += HEXAGON_SPEED * deltaTime;

        if (hexagons.life[i] == 0 || (oldLife < 0 && hexagons.life[i] > 0))
        {
            score++;
            if (score > highScore)
                highScore = score;
        }

        if (hexagons.life[i] >= HEXAGON_AMOUNT / 2.0)
        {
            ResetHexagon(i);
        }

        if (hexagons.life[i] >= -1 && hexagons.life[i] <= 1)
        {
            for (int j = 0; j < 6; j++)
            {
                if (j != hexagons.start[i] && j != hexagons.end[i])
                {
                    Vector2 p1 = PointFromRotation(j, hexagons.life[i], rotation);
                    Vector2 p2 = PointFromRotation(j - 1, hexagons.life[i], rotation);

                    if (CheckCollisionPointLine((Vector2) { 0, 0 }, p1, p2, PLAYER_SIZE * 2))
                    {
                        EndGame();
                    }
                }
            }
        }
    }
}

static void Update(void)
{
    float deltaTime = GetFrameTime();
    UpdateMusicStream(music);

    Logic(deltaTime);
    Draw(deltaTime);
}

static void StartGame(void)
{
    state = Playing;
    rotation = 0;
    score = 0;

    hexagons.life[0] = -HEXAGON_AMOUNT / 2.0;
    hexagons.start[0] = 2;
    hexagons.end[0] = 5;

    for (int i = 1; i < HEXAGON_AMOUNT; i++)
    {
        ResetHexagon(i);
        hexagons.life[i] -= i;
    }

}

static void EndGame(void)
{
    state = Dead;
    deadTime = 0;
}

int main(void)
{
    SetConfigFlags(FLAG_WINDOW_RESIZABLE);
    InitWindow(DEFAULT_SCREEN_WIDTH, DEFAULT_SCREEN_HEIGHT, "Linux Game Jam");

    InitAudioDevice();

    sideModel = LoadModelFromMesh(GenSideMesh());
    music = LoadMusicStream("resources/music.mp3");

    PlayMusicStream(music);

    time_t t;
    srand((unsigned) time(&t));

#if defined(PLATFORM_WEB)
    emscripten_set_main_loop(Update, 60, 1);
#else
    SetTargetFPS(60);

    while (!WindowShouldClose())
    {
        Update();
    }
#endif

    UnloadModel(sideModel);

    CloseAudioDevice();

    CloseWindow();

    return 0;
}

static void ResetHexagon(int id)
{
    hexagons.life[id] = -HEXAGON_AMOUNT / 2.0;
    hexagons.start[id] = (hexagons.end[id != 0 ? id - 1 : HEXAGON_AMOUNT - 1] + 3) % 6;
    hexagons.end[id] = (rand() % 5 + 1 + hexagons.start[id]) % 6;
}

static Vector2 PointFromRotation(int segment, float life, float rotation)
{
    rotation *= PI / 180;

    if (segment < 0)
        segment += 6;

    return (Vector2) {
        -sin(rotation + segment * PI / 3) * HEXAGON_APOTHEM + life * HEXAGON_SPACING * -10,
        -cos(rotation + segment * PI / 3) * HEXAGON_APOTHEM
    };
}

static Mesh GenSideMesh(void)
{
    Mesh mesh = { 0 };
    mesh.triangleCount = 2;
    mesh.vertexCount = 4;

    mesh.vertices = malloc(mesh.vertexCount * 3 * sizeof(float));

    mesh.vertices[0] = cos(PI / 6) * (HEXAGON_APOTHEM + HEXAGON_THICKNESS / 2);
    mesh.vertices[1] = 0;
    mesh.vertices[2] = sin(PI / 6) * (-HEXAGON_APOTHEM - HEXAGON_THICKNESS / 2);

    mesh.vertices[3] = 0;
    mesh.vertices[4] = 0;
    mesh.vertices[5] = -HEXAGON_APOTHEM - HEXAGON_THICKNESS / 2;

    mesh.vertices[6] = cos(PI / 6) * (HEXAGON_APOTHEM - HEXAGON_THICKNESS / 2);
    mesh.vertices[7] = 0;
    mesh.vertices[8] = sin(PI / 6) * -(HEXAGON_APOTHEM - HEXAGON_THICKNESS / 2);

    mesh.vertices[9] = 0;
    mesh.vertices[10] = 0;
    mesh.vertices[11] = -HEXAGON_APOTHEM + HEXAGON_THICKNESS / 2;

    mesh.indices = malloc(mesh.triangleCount * 3 * sizeof(unsigned short));
    
    mesh.indices[0] = 0;
    mesh.indices[1] = 1;
    mesh.indices[2] = 2;

    mesh.indices[3] = 2;
    mesh.indices[4] = 1;
    mesh.indices[5] = 3;

    // Upload mesh data from CPU (RAM) to GPU (VRAM) memory
    UploadMesh(&mesh, false);

    return mesh;
}
